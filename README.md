
# How To Install ?

Simple :
* Download latest release zip file : https://gitlab.com/GreatNameAgain/diamond-mod-bb/repository/19.09.2017.B/archive.zip
* Uncompress in BB root directory. You can choose the content to unzip. I added my named save files as well if you want to enjoy.

Advanced :
* Install Git
* Clone the directory into your bb folder for easy updates, for this open a command line then browse to the BB directory.
* `git clone https://gitlab.com/GreatNameAgain/diamond-mod-bb.git .`
* to update simply do `git pull`

### Special save files instruction

Before extracting you'll have to backup your save somewhere and clean your save directory

### Compatibility & Merging with other mods

Should be working with other mods based on Mod.txt like Chancer and IKARUGA. You have to copy the content of Diamond's Mod.txt after `# - DIAMOND Added sex scenes` and the images.


# Content Spoils :


| Title | Description | Where to find it | Conditions | Credits |
| :-----: | ----------- | ---------------- | ------------|------|
| Mom I can't sleep Again | Ask mom to watch TV at night with you | Mom room 1 or 2 am |  Mom must be lustful enough, unlocks after alice catch you with mom and kira watching the movie |[skyphernaut]
| Kira and Alice afternight | Kira and Alice are having fun in bath after the night club | Bathroom saturday 8am || [FunFiction]
| Punish Alice in bathroom | You can now punish alice at night in bath. Don't push too much .. otherwise | Midnight alice in bathroom|Ask mom to punish alice this day and wait for midnight, don't do it on fridays|[celtic1138]
|Alice after club enhanced |Extended option when Alice comes back drunk from the club|You can now enter Alice from the 3 options in shower| Alice drunk from nightclub|[celtic1138]
| Are you tired Mom ?| A new opportunity when massaging mom | Mom in her room, depending on Alice and Lisa's position you'll have some variations | Mom must be lustful enough, unlocks after alice catch you with mom and kira watching the movie | [Iceridlah][Rich]
| Have a peek in mom's room at night | Let you turn on the light when mom and kira are sleeping | Moms room with kira at 3am | Mom blown you once | [FunFiction]
| Max wants to go jogging | Max goes for a jogging, meanwhile kira is somehow angry against mom | Tell mom you know what you want ! |  Mom must be lustful enough, unlocks after alice catch you with mom and kira watching the movie | [FunFiction]
| BETA - Have a peek in mom's room in the morning  | Let you sneak in to kira when she's sleeping | Moms room with kira alone at 6am | Kira naked | [?]
| Why so gloomy Alice ? | Let Alice enjoy or be punished when she's gloomy | When alice is at the terrace | You should ask to private punish alice in the morning if you want the punish option | [celtic1138]
| Peep and open the door | Lisa is the best before school | 10am during the week at bedroom | after kira 3rd lesson | [skyphernaut]
| New pair of Lisa images | Because she can be even more sexy | mostly 6am and 8am in bedroom | Sleeping naked and wearing no panties at home  |[skyphernaut]


#Changelog

19.09.2017.B :
* Added can't sleep again
* Added Kira and Alice having fun

19.09.2017.A :
* Added Alice punishment in bathroom
* Added extended sex when alice comes back drunk from the club
* Removed others from Mods.txt to better handle merging
* Lots of fixes to other dialogues

11.09.2017 :
* First entry of the change log
* Added "Max Wants to go jogging" opportunity
* Lisa 10am opportunity now have more Fan Arts.
* Other opportunities that are implemented:
    * Mom and Kira at night 3am
    * Kira in the morning 6am
    * Lisa has some updated pictures
    * You can punish Alice at the pool if you ask mom in the morning

# Todo

* Let's spank Alice more and do a better loung stuff